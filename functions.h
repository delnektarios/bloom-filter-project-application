
//  NEKTARIOS DELIGIANNAKIS 1115201200030 sdi1200030@di.uoa.gr

#ifndef FUNCTIONS //include guard

#define NUM 3;
#define MAX_SIZE 140000; //Bytes for bloom filter

#define FUNCTIONS

typedef struct tree_node *node_pointer; //pointer to tree node

//definition of tree node
typedef struct tree_node{
	int existing_depth,number_of_children;
	char *word;
	node_pointer *table_of_children;
}tree_node;


void ask_oracle(int number_of_bits,char *ptr,int num,int TREE_DEPTH,char *word_for_oracle,char **word_found,node_pointer *root,int existing_depth);

int bloom_filter(int number_of_bits,uint64_t value_from_hash_by[],int num,unsigned char* word);

void create_tree(node_pointer *pointer);

void create_node(node_pointer *node,int number_of_children,int len_word,int depth,char *word);

void insert_node(node_pointer *root,node_pointer *node,int position);

void print_and_destroy_tree(node_pointer *node,int print_or_not);

#endif
