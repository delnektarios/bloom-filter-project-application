
//  NEKTARIOS DELIGIANNAKIS 1115201200030 sdi1200030@di.uoa.gr

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <string.h>
#include <limits.h>
#include "hash.h"
#include "functions.h"

//TREE IMPLEMENTATION
//----------------------------------------------------------------------------------------------------------

//creating tree
void create_tree(node_pointer *pointer){
	*pointer=NULL;
        return;
}

//create node
void create_node(node_pointer *node,int number_of_children,int len_word,int depth,char *word){
        int i;
        *node=malloc(sizeof(tree_node)); //allocate space for the node to be inserted to tree
        if((*node)==NULL) exit(3);

        //initializing members
        (*node)->existing_depth=depth;
        (*node)->number_of_children=number_of_children;

        (*node)->word=malloc(len_word+1); //allocate space for word
        if((*node)->word==NULL)
        	{printf("FAILED\n"); exit(3);}
        strcpy((*node)->word,word);  //copy word in member word

        //allocating memory for the table of pointer for the children nodes
        (*node)->table_of_children=malloc(number_of_children*sizeof(node_pointer));
        if((*node)->table_of_children==NULL) exit(3);
        for(i=0; i<number_of_children; i++) (*node)->table_of_children[i]=NULL;
        return; 
}
//insert node to tree
void insert_node(node_pointer *root,node_pointer *node,int position){
        (*root)->table_of_children[position]=*node;
        return;
}

//print and destroy tree
void print_and_destroy_tree(node_pointer *node,int print){
        int i;
        if(*node==NULL) return;  //in case of an empty tree
        //if the word is found variable "print" allows the print of tree
        if(print){
                printf("%s\n\n\nPARENT NODE--->",(*node)->word);
                for(i=0; i<((*node)->number_of_children); i++){
                        if((*node)->table_of_children[i]==NULL) continue;
                        printf("%s -->",((*node)->table_of_children[i])->word);
                }
                printf("NULL\n");
        }
        //visiting recursivelly the children nodes
        for(i=0; i<((*node)->number_of_children); i++){
                if((*node)->table_of_children[i]==NULL) continue;
                print_and_destroy_tree(&((*node)->table_of_children[i]),print);
        }     
        //when the recursion starts to return, it frees members on its way           
        free((*node)->table_of_children);       //freeing table
        free((*node)->word);                    //freeing word
        free(*node);                            //freeing the node itself
        return;
}
//END OF TREE IMPLEMENTATION
//----------------------------------------------------------------------------------------------

// BLOOM FILTER
int bloom_filter(int N,uint64_t value[],int num,unsigned char *ptr){
        int position,check,exists=0,i;
        unsigned char val;
        for(i=0; i<num; i++){   //for the number of hash functions
                position=value[i]%N;    // (value) mod (number of bits in table) = position of the desired bit
		//val is used to have the desired bit in the position we want
		val=pow(2,position%CHAR_BIT);
                // use the operator & to check if the bit you need is 1
                check=val&ptr[position/CHAR_BIT];
                //use the operator | to change the wanted bit to 1
	        if(!check) ptr[position/CHAR_BIT]=val|ptr[position/CHAR_BIT];
                if(check) exists++; //if all the bits are 1,the word exists in bloom filter
        }
        if(exists==num) return 1; //if all the bits are 1,the word exists in bloom filter
        return 0;                 //otherwise,it does not exist
}

//this function runs the recursion
void ask_oracle(int N,char *p,int num,int depth,char *word,char **word_found,node_pointer *root,int d){
        //asking oracle a word
        const char **table=oracle(word);
        //oracle answers
        if(table==NULL) { //WORD FOUND
                *word_found=malloc(strlen(word)+1); 
                strcpy(*word_found,word); //KEEPING THE HIDDEN WORD
                return; 
        }
        
        if(table[0]==NULL) return; //no answers given from oracle

        d++; //incrementing existing depth of recursion and of the tree
        int check=0,i,number_of_strings=0,max=0,position=0,t,found;
        uint64_t value[num];    
        node_pointer node=NULL;
        char **s; //table of strings to copy the answers from oracle
        
        while (table[number_of_strings]!=NULL)	//counting the lines
        	 number_of_strings++;
        
        s=(char**)malloc((number_of_strings+1)*sizeof(char*)); //allocating lines
        if(s==NULL) exit(2);
        s[number_of_strings]=NULL; 
			
        int j=number_of_strings,k=0;
					  
        for(i=0; i<j; i++){ //calculating the size of biggest string to allocate enough memory

        		while(table[i][k]!='\0')
        			k++;
        					
        		s[i]=(char*)malloc((k+1)*sizeof(char));
                        if(s[i]==NULL) exit(2);
                        if(k>max) 
                        	max=k;
        }
        for(i=0; table[i]!=NULL; i++)
                strcpy(s[i],table[i]); //copying tables
        if(d==1){
                create_node(&(*root),number_of_strings,max,depth,word); //creating the root node only once
        }
        //for(i=0; s[i]!=NULL; i++) printf("(%s)\n",s[i]);

        // for each answer
        for(i=0; s[i]!=NULL; i++){
                for(t=0; t<num; t++) // calling the hush functions
                        value[t]=hash_by(t,s[i]);
                check=bloom_filter(N,value,num,p); //checking with bloom filter

                if(check) continue; //if the word exists in the filter, bypass it

                if(d<=depth){ //if depth not reached create and store a new node to the tree
                        create_node(&node,number_of_strings,max,depth,s[i]);
                        insert_node(&(*root),&node,position);
                }
                ask_oracle(N,p,num,depth,s[i],word_found,&node,d);
                position++;
        }
        for(i=0; i<j; i++)
                free(s[i]);
        free(s);
        return;
         
}
