
//  NEKTARIOS DELIGIANNAKIS 1115201200030 sdi1200030@di.uoa.gr

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <limits.h>
#include "oracle.h"
#include "functions.h"


int main(int argc,char *argv[]){
        system("clear"); //cleaning screen
        srand(time(NULL)); //initializing rand()

	//always call them before initSeed();
	//setEasyMode(void);
	//setHardMode(void);

        int num=3;         //default value for number of hush functions
        int i=0,check=0,size=131000,depth=10,d=0;
        char *p,*found=NULL,*word=" ";
        node_pointer root=NULL; //root of tree
        if(argc<4) exit(1);     //not enough arguments
        if(argc==5){            //in case all argument are given re-initialize num,size and depth
               num=atoi(argv[2]);
               size=atoi(argv[3]);      //size of bloom filter in bytes
               depth=atoi(argv[4]);     //what depth te tree must reach
        }       
        initSeed(rand()); //initializing oracle
        // get memory for the bloom filter and you get size*CHAR_BIT bits 
        p=(unsigned char *)malloc(size*sizeof(char));
        if(p==NULL) exit(2); //EXIT CODE 2 SUGGESTS MALLOC FAILED
        for (i=0; i<size; i++) p[i]=0; //initializing bloom filter array of bits

        ask_oracle(size*CHAR_BIT,p,num,depth,word,&found,&root,d); //begin the recursion

        printf("\n");
        if(found!=NULL){ //found "travels" through the whole recursion to kepp the hidden word,if found
                printf("FOUND THE WORD = (%s)\n",found);
                print_and_destroy_tree(&root,1); //the second argument is to 1 ,so the tree will be printed
                printf("\n\nFOUND THE WORD = (%s)\n",found);
        }
        else{
                printf("FAILED TO FIND THE WORD\n");
                print_and_destroy_tree(&root,0); //the second argument is to 0 ,the tree will only be dstroyed
        }
        sleep(0.5);
        printf("\n");
        return 0;
}

