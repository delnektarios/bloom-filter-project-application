CC=gcc

CFLAGS=-c -w

LIBS=-L . -loracle_v2 -lhash -lm

all: execute
	rm -rf *o invoke-oracle

execute: invoke-oracle
	./invoke-oracle -k 5 13100 10

invoke-oracle: oracle.o functions.o
	$(CC) oracle.o functions.o -o invoke-oracle $(LIBS)

oracle.o: 
	$(CC) $(CFLAGS) oracle.c $(LIBS)

functions.o: 
	$(CC) $(CFLAGS) functions.c $(LIBS)
